//
//  DisassemblyInfo.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 13/05/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Foundation
import Toolbox

private let log = Logger.getLogger("VisualDisassembler.DisassmeblyInfo")

struct DisassemblyInfo
{

	var info: [Info]
	private(set) var bookmark: Data?
	private(set) var programUrl: URL?
	private var urlIsFromBookmark = false


	/// Enables the URL for use to read a program file
	/// - Parameter block: Block to execute, it takes a security enabled
	///                    URL as a parameter
	/// - Returns: whatever is returned from the block.
	/// - Throws: if the bookmark is invalid in some way or the block throws
	mutating func executeWithProgramUrl<T>(documentUrl: URL?, block: (URL) throws ->T) throws -> T
	{
		log.pushLevel(.debug)
		defer { log.popLevel() }
		if urlIsFromBookmark
		{
			do
			{
				guard var data = bookmark else { throw Document.Error.infoHasNoBookmark }
				var isStale = false
				var url = try URL(resolvingBookmarkData: data,
								  options: [.withSecurityScope],
								  relativeTo: documentUrl,
								  bookmarkDataIsStale: &isStale)
				if isStale
				{
					let accessing = url.startAccessingSecurityScopedResource()
					defer
					{
						if accessing
						{
							url.stopAccessingSecurityScopedResource()
						}
					}
					data = try url.bookmarkData(options: [.withSecurityScope], relativeTo: documentUrl?.absoluteURL)
					url = try URL(resolvingBookmarkData: data,
								  options: [.withSecurityScope],
								  relativeTo: documentUrl,
								  bookmarkDataIsStale: &isStale)
					self.bookmark = data
				}
				if isStale
				{
					log.error("Could not make bookmark for \(url) unstale")
					throw Document.Error.bookmarkIsStale
				}
				programUrl = url
				guard url.startAccessingSecurityScopedResource()
					else { log.debug("Failed to access \(url)") ; throw Document.Error.bookmarkInvalid }
				defer { url.stopAccessingSecurityScopedResource() }
				log.debug("Running block on \(url)")
				return try block(url)
			}
			catch
			{
				log.error("\(error)")
				throw error
			}
		}
		else
		{
			guard let url = programUrl else { throw Document.Error.infoHasNoUrl }
			return try block(url)
		}
	}

	init(info: [Info] = [])
	{
		self.info = info
		self.urlIsFromBookmark = false
	}

	private mutating func set(bookmark newBookmark: Data) throws
	{
		var bookmarkIsStale: Bool = false
		let newUrl = try URL(resolvingBookmarkData: newBookmark, bookmarkDataIsStale: &bookmarkIsStale)
		if bookmarkIsStale
		{
//					self.bookmark = try newUrl.bookmarkData(options: [.withSecurityScope])
		}
		log.debug("New program url will be \(newUrl)")
		self.programUrl = newUrl
		log.debug("Set program Url to \(self.programUrl?.description ?? "nil")")
		self.bookmark = newBookmark
		urlIsFromBookmark = true
	}

	mutating func set(programUrl: URL, documentUrl: URL?) throws
	{
		self.bookmark = try programUrl.bookmarkData(options: [.withSecurityScope], relativeTo: documentUrl?.absoluteURL)
		self.programUrl = programUrl
		self.urlIsFromBookmark = false
	}
}


extension DisassemblyInfo: Codable
{
	enum CodingKeys: CodingKey
	{
		case info
		case bookmark
	}

	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		if let bookmark = bookmark
		{
			try container.encode(bookmark, forKey: .bookmark)
		}
		try container.encode(info, forKey: .info)
	}

	init(from decoder: Decoder) throws
	{
		let values = try decoder.container(keyedBy: CodingKeys.self)
		self.info = try values.decode([Info].self, forKey: .info)
		if try values.contains(.bookmark) && !values.decodeNil(forKey: .bookmark)
		{
			let value = try values.decode(Data.self, forKey: .bookmark)
			try set(bookmark: value)
		}
	}
}

extension DisassemblyInfo: Equatable
{
	static func == (lhs: DisassemblyInfo, rhs: DisassemblyInfo) -> Bool
	{
		return lhs.programUrl == rhs.programUrl && lhs.info == rhs.info
	}
}
