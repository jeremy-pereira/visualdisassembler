//
//  M6502.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 12/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// A disassembler for Mostek 6502
struct M6502
{
	private init() {}

	/// Convenience function to create a 6502 disassembler that uses CA65 format
	/// - Returns: A disassembler
	static func makeCA65Disassembler() -> Disassembly<M6502.Disassembler, M6502.CA65Writer>
	{
		let writer = M6502.CA65Writer()
		let parser = M6502.Disassembler(symbolTableDelegate: writer)
		return Disassembly(parser: parser, codeWriter: writer)
	}
}


extension M6502
{
	enum Error: DisassemblyError
	{
		/// No start address specified in the info
		case noStartAddress
		/// A duplicate global section was found
		///
		/// The duplicate will be ignored.
		case duplicateGlobal

		case invalidDataType(String)

		var effect: ErrorEffect
		{
			switch self
			{
			case .noStartAddress: return .fatal
			case .duplicateGlobal: return .warning
			case .invalidDataType: return .warning
			}
		}
	}
}
