//
//  SymbolTable.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 17/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Toolbox

/// Protocol describing delegate functions for a symbol table
///
/// This is mainly for manipulating addresses in various ways.
protocol SymbolTableDelegate
{
	/// A type for addresses
	///
	/// The address type must be `Comparable` because the `SymbolTable` uses it as
	/// a key for an ordered array.
	associatedtype Address: Comparable

	/// Turn the label into a string suitable to be a label
	/// - Parameter address: The address to stringify
	/// - Returns: A string suitable to be a label
	func stringifyForLabel(address: Address) -> String

	/// Try to create a unique version of a symbol name that already exists
	///
	/// You don't have to ensure  the returned name doesn't exist, just provide
	/// a legitimate candidate. Note that returning the same name will result in an
	/// infinite loop.
	///
	/// For example, add a suffix with an ordinal e.g.
	///
	/// ```
	/// existing_symbol   -> existing_symbol_0
	/// existing_symbol_0 -> existing_symbol_1
	/// ```
	/// etc
	/// - Parameter name: The symbol to uniquify
	/// - Returns:
	func uniquify(_ name: String) -> String

	/// Create an address from a string representation from an info
	/// - Parameter info: The string form of the address
	/// - Returns: An address
	/// - Throws: if the string cannot be converted to an address
	func addressFrom(info: String) throws -> Address
}
/// A symbol table
///
/// Each symbol has a unique name and refers to an address
struct SymbolTable<Address: Comparable>
{

	private var symbolNamesByAddress = OrderedArray<(Address, String), Address>([], keyFunction: { $0.0 })
	private var symbolsByName: [String: Symbol<Address>] = [:]

	var allSymbols: [Symbol<Address>] { Array(symbolsByName.values) }

	private let delegate: AnySymbolTableDelegete<Address>

	init<Delegate: SymbolTableDelegate>(delegate: Delegate)
	where Delegate.Address == Address
	{
		self.delegate = AnySymbolTableDelegete<Address>.wrap(delegate)
	}

	subscript(index: Address) -> [Symbol<Address>]
	{
		guard var i = symbolNamesByAddress.index(ofKey: index) else { return [] }
		var ret: [Symbol<Address>] = []
		while i < symbolNamesByAddress.count && symbolNamesByAddress[i].0 == index
		{
			let name = symbolNamesByAddress[i].1
			ret.append(symbolsByName[name]!)
			i += 1
		}
		return ret
	}

	subscript(range: ClosedRange<Address>) -> [Symbol<Address>]
	{
		var (i, _) = symbolNamesByAddress.findLowestIndexOf(range.lowerBound)
		var ret: [Symbol<Address>] = []
		while i < symbolNamesByAddress.count && symbolNamesByAddress[i].0 <= range.upperBound
		{
			let name = symbolNamesByAddress[i].1
			ret.append(symbolsByName[name]!)
			i += 1
		}
		return ret
	}

	subscript(name: String) -> Symbol<Address>?
	{
		return symbolsByName[name]
	}



	/// True if a symbol with the given name exists
	/// - Parameter name: The name to check
	/// - Returns: true if a symbol with the given name exists
	func hasSymbol(named name: String) -> Bool
	{
		return symbolsByName[name] != nil
	}

	/// Automatically create a symbol from an address
	/// - Parameters:
	///   - address: The address to which the symbol refers
	///   - isDefinitelyCode: True if the symbol represents the start of an instruction
	///   - comment: A comment that can be used in a printed table of symbols
	mutating func autoCreate(address: Address, isDefinitelyCode: Bool, comment: String?)
	{
		var baseName = delegate.stringifyForLabel(address: address)
		while self.hasSymbol(named: baseName)
		{
			baseName = delegate.uniquify(baseName)
		}
		let symbol = Symbol(name: baseName,
							address: address,
							autoCreated: true,
							isDefinitelyCode: isDefinitelyCode,
							comment: comment)
		symbolsByName[baseName] = symbol
		symbolNamesByAddress.insert((address, baseName))
	}

	/// Add a symbol from the info for the program.
	///
	///
	/// - Parameters:
	///   - name: The symbol's name
	///   - address: The address to which it refers as a string. The delegate
	///              will convert it to an actual address/
	///   - isCode: True if the symbol represents the start of an instruction
	///   - comment: A comment that can be added to the printed output
	/// - Throws: If the address string cannot be converted to an address
	mutating func addFromInfo(name: String, address: String, isCode: Bool, comment: String?) throws
	{
		let realAddress = try delegate.addressFrom(info: address)
		let symbol = Symbol(name: name,
							address: realAddress,
							autoCreated: false,
							isDefinitelyCode: isCode,
							comment: comment)
		symbolsByName[name] = symbol
		symbolNamesByAddress.insert((realAddress, name))
	}

	func addressFrom(info: String) throws -> Address
	{
		return try delegate.addressFromInfo(info)
	}
}

/// Represents a symbol
struct Symbol<Address>
{
	/// The textual name of the symbol as it appears in the source code file
	let name: String
	/// The address to which the symbol refers
	let address: Address
	/// True if the disassembler created it automatically
	let autoCreated: Bool
	/// True if the address is one to which the program jumps during execution
	let isDefinitelyCode: Bool
	/// A textual comment for the symbol that can appear in the output source
	let comment: String?
}

/// A type erased symbol table delegate
struct AnySymbolTableDelegete<Addr: Comparable>: SymbolTableDelegate
{
	private init<Delegate: SymbolTableDelegate>(wrapped: Delegate)
		where Delegate.Address == Addr
	{
		self.stringify = { (addr: Addr) -> String in wrapped.stringifyForLabel(address: addr) }
		self.uniquify = { wrapped.uniquify($0) }
		self.addressFromInfo = { try wrapped.addressFrom(info: $0) }
	}

	static func wrap<D: SymbolTableDelegate>(_ wrappee: D) -> AnySymbolTableDelegete<D.Address>
	{
		return (wrappee as? AnySymbolTableDelegete<D.Address>) ?? AnySymbolTableDelegete<D.Address>(wrapped: wrappee)
	}

	let stringify: (Addr) -> String
	let uniquify: (String) -> String
	let addressFromInfo: (String) throws -> Address

	func stringifyForLabel(address: Addr) -> String
	{
		stringify(address)
	}

	func uniquify(_ name: String) -> String
	{
		uniquify(name)
	}

	func addressFrom(info: String) throws -> Addr
	{
		return try addressFromInfo(info)
	}
}

extension Symbol
{
	enum Error: DisassemblyError
	{
		case invalidAddress(String)

		var effect: ErrorEffect
		{
			switch self
			{
			case .invalidAddress:
				return .warning
			}
		}

	}
}
