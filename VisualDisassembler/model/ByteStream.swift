//
//  ByteStream.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 29/03/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


/// A stream of bytes with lookahead
class ByteStream
{
	private var sequenceIterator: AnyIterator<UInt8>
	private var alreadyGot: [UInt8] = []

	init<S: Sequence>(sequence: S) where S.Element == UInt8
	{
		sequenceIterator = AnyIterator(sequence.makeIterator())
		
	}

	/// Get the byte at the given index
	///
	/// If the stream doesn't have a byte at the given index we return `nil`
	/// - Parameter index: The index of the byte to get
	/// - Returns: The byte or `nil` if it's after the end of the stream.
	subscript(index: Int) -> UInt8?
	{
		while alreadyGot.count <= index
		{
			guard let nextByte = sequenceIterator.next() else { return nil }
			alreadyGot.append(nextByte)
		}
		return alreadyGot[index]
	}

	/// Discard the first number of bytes from the stream
	/// - Parameter count: Number of bytes to discard
	func discard(count: Int)
	{
		if count <= alreadyGot.count
		{
			alreadyGot.removeFirst(count)
		}
		else
		{
			let streamCount = count - alreadyGot.count
			alreadyGot = []
			for _ in 0 ..< streamCount
			{
				guard sequenceIterator.next() != nil else { break }
			}
		}
	}

	/// Return the next byte in the stream and consume it
	/// - Returns: The next byte in ithe stream
	func next() -> UInt8?
	{
		if alreadyGot.count > 0
		{
			return alreadyGot.removeFirst()
		}
		else
		{
			return sequenceIterator.next()
		}
	}
}

/// Orders bytes from a stream according to an array of indexes
///
/// The idea is to make it easier to manipulate big and little endian sequences.
///
/// It is assumed that the indexes in a byte order are unique and are contiguous
/// from zero. `[3, 2, 0, 1]` would be valid but not `[1, 2, 3, 4]` or
/// `[0, 1, 1, 3]`
struct ByteOrder
{
	let order: [Int]

	init(_ order: [Int])
	{
		// Check for a valid sequence
		guard order.sorted().enumerated().first(where: { $0.0 != $0.1 }) == nil
			else { fatalError("Byte order '\(order)' breaks the rules") }
		self.order = order
	}

	var  count: Int { order.count }


	/// Returns an array of the prefix bytes of the stream in the right order
	///
	/// For example, the byte order `[1, 0]` will return the second byte in the
	/// stream and the first one. If the stream is not long enough, `nil` is
	/// returned.
	///
	/// This is a non destructive operation on the stream.
	///
	/// - Parameters:
	///   - stream: Stream to get bytes from
	///   - offset: How far from the start to get the bytes
	/// - Returns: The bytes in the right order.
	func bytes(stream: ByteStream, offset: Int) -> [UInt8]?
	{
		let ret = order.compactMap
		{
			stream[$0 + offset]
		}
		return ret.count == order.count ? ret : nil
	}
	/// Returns the prefix bytes of the stream in a `UInt64`
	///
	/// This is the same as taking `bytes(stream:)`and shifting them into a
	/// `UInt64` in little endian order i.e. the first byte in the stream will
	/// go into the bottom byte of the `UInt64`. The return value will be padded
	/// with zeros, or truncated if the byte order coount is less than 8 or
	/// greater than 8 respecively.
	///
	/// This is a non destructive operation on the stream.
	///
	/// - Parameters:
	///   - stream: Stream to get bytes from
	///   - offset: How far into the stream to get the bytes
	/// - Returns: The bytes in a `UInt64` or `nil` if the end of the stream is
	///            encountered.
	func uint64(stream: ByteStream, offset: Int) -> UInt64?
	{
		let bytesInUInt64 = UInt64.bitWidth / UInt8.bitWidth
		guard let bytes = self.bytes(stream: stream, offset: offset) else { return nil }
		let firstEight = bytes.prefix(bytesInUInt64)
		var ret: UInt64 = 0
		for i in 0 ..< firstEight.count
		{
			ret |= UInt64(firstEight[i]) << (i * UInt8.bitWidth)
		}
		return ret
	}
	/// Returns the prefix bytes of the stream in a `UInt16`
	///
	/// This is the same as taking `bytes(stream:)`and shifting them into a
	/// `UInt16` in little endian order i.e. the first byte in the stream will
	/// go into the bottom byte of the `UInt64`. The return value will be padded
	/// with zeros, or truncated if the byte order coount is less than 2 or
	/// greater than 2 respecively.
	///
	/// This is a non destructive operation on the stream.
	///
	/// - Parameters:
	///   - stream: Stream to get bytes from
	///   - offset: How far into the stream to get the bytes
	/// - Returns: The bytes in a `UInt16` or `nil` if the end of the stream is
	///            encountered.
	func uint16(stream: ByteStream, offset: Int) -> UInt16?
	{
		let bytesInUInt16 = UInt16.bitWidth / UInt8.bitWidth
		guard let bytes = self.bytes(stream: stream, offset: offset) else { return nil }
		let firstTwo = bytes.prefix(bytesInUInt16)
		var ret: UInt16 = 0
		for i in 0 ..< firstTwo.count
		{
			ret |= UInt16(firstTwo[i]) << (i * UInt8.bitWidth)
		}
		return ret
	}

	/// Single byte bit sequence
	static let byte: ByteOrder = [0]
	/// little endian 16 bit
	static let littleEndian16: ByteOrder = [0, 1]
	/// Big endian 16
	static let bigEndian16: ByteOrder = [1, 0]

}

extension ByteOrder: ExpressibleByArrayLiteral
{
	init(arrayLiteral elements: Int...)
	{
		self.init(elements)
	}
}
