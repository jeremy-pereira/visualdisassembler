//
//  RangeTable.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 27/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// Describes a collection of ranges which define the type of bytes at
/// particular addresses
///
/// Ranges must not overlap and this rule is enforced when adding new ranges.
/// If  there is no range for a particular address, the default type specified
/// at initialisation is used.
struct RangeTable<Address: Addressing, ByteType>
{
	private var ranges = OrderedArray<ByteRange<Address, ByteType>, Address>([],
																			 keyFunction: { $0.range.lowerBound } )
	private var defaultType: ByteType

	init(defaultType: ByteType)
	{
		self.defaultType = defaultType
	}

	mutating func insert(range: ClosedRange<Address>, type: ByteType) throws
	{
		let newRange = ByteRange(range: range, type: type)
		let (insertionPoint, _) = ranges.findLowestIndexOf(newRange.range.lowerBound)
		// Now check that the new range does not overlap the ranges above and below it
		guard insertionPoint == ranges.count || !ranges[insertionPoint].overlaps(newRange)
		else { throw ByteRangeError.overlappingRanges }
		guard insertionPoint == 0 || !ranges[insertionPoint - 1].overlaps(newRange)
		else { throw ByteRangeError.overlappingRanges }
		ranges.insert(newRange, allowDuplicates: false)
	}

	/// Find the range containing the given address
	///
	/// If no range has been defined, a range is constructed from the gap with
	/// the default type
	///
	/// - Parameter address: The address whose range is to be found
	/// - Returns: The range containing the address
	func range(of address: Address) -> ByteRange<Address, ByteType>
	{
		let (index, exists) = ranges.findLowestIndexOf(address)
		// Optimisation: if the address is the first address in a range, get it
		// out of the way
		guard !exists else { return ranges[index] }
		// If it's in a range, it's going to be the one before the insertion point
		guard index == 0 || !ranges[index - 1].contains(address)
		else { return ranges[index - 1] }
		// We have to make up a new range
		// The lower bound of the range is either the upper bound of the
		// previous range (which cannot be the highest address or we would have
		// in that range) or the lower bound of all addresses.
		//
		// The upper bound of the new range is either the lower bound of the
		// next range or the upper bound of all addresses.
		let start = index == 0 ? Address.range.lowerBound
							   : ranges[index - 1].range.upperBound.successor!
		let end = index == ranges.count ? Address.range.upperBound
							            : ranges[index].range.lowerBound.predecessor!
		return ByteRange(range: start ... end, type: defaultType)
	}

}

/// A range of bytes
///
/// Byte ranges are defined by the first and  last address of them. A byte
/// range cannot be empty as a result.
struct ByteRange<Address: Addressing, ByteType>
{
	/// The addresses in this range
	let range: ClosedRange<Address>
	/// The number of bytes in the range
	var byteCount: Int { range.lowerBound.bytes(to: range.upperBound) + 1}
	/// The type of data in the rqnge
	let type: ByteType

	init(range: ClosedRange<Address>, type: ByteType)
	{
		self.range = range
		self.type = type
	}

	/// Test if another range overlaps this range
	/// - Parameter other: The other range
	/// - Returns: `true` if the two ranges have at least one address in common
	func overlaps(_ other: ByteRange<Address, ByteType>) -> Bool
	{
		self.range.overlaps(other.range)
	}

	/// Test if an address is in this range
	/// - Parameter address: The address to test
	/// - Returns: True if this rtange contains the address
	func contains(_ address: Address) -> Bool
	{
		return range.contains(address)
	}
}

enum ByteRangeError: DisassemblyError
{
	var effect: ErrorEffect
	{
		switch self
		{
		case .invalidDelimiters, .overlappingRanges: return .warning
		}
	}

	case invalidDelimiters
	case overlappingRanges
}

extension ClosedRange
{
	init(startAddress: Bound, endAddress: Bound) throws
	{
		guard startAddress <= endAddress else { throw ByteRangeError.invalidDelimiters }
		self.init(uncheckedBounds: (lower: startAddress, upper: endAddress))
	}
}
