//
//  Disassembler.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 05/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

private let log = Logger.getLogger("VisualDisassembler.Disassembly")

/// Protocol that must be adopted by addresses in the particular code type
///
/// Addresses need to be comparable because symbol tables and  range tables
/// store symbols and ranges as ordered lists.
protocol Addressing: Comparable
{
	/// Find the number of bytes between this address and another address
	///
	/// If the other address is less than `self` the count will be negative.
	///
	/// For address types that conform to `Strideable where Stride == Int` a
	/// default implementation exists in terms of `distance(to:)`
	///
	/// - Parameter to: Another address
	/// - Returns: The count of bytes between this address and the other address
	func bytes(to: Self) -> Int

	/// Describes the range of values an address can take
	///
	/// A default implementation exists for `FixedWidthInteger`
	static var range: ClosedRange<Self> { get }
	/// The address before this one, if any
	///
	/// If the type conforms to `Strideable where Stride == Int` a default
	/// implementation is provided
	var predecessor: Self? { get }
	/// The address after this one, if any
	///
	/// If the type conforms to `Strideable where Stride == Int` a default
	/// implementation is provided
	var successor: Self? { get }
}

extension Addressing where Self: Strideable, Self.Stride == Int
{
	func bytes(to: Self) -> Int
	{
		return self.distance(to: to)
	}

	var predecessor: Self?
	{
		guard self > Self.range.lowerBound else { return nil }
		return self.advanced(by: -1)
	}
	var successor: Self?
	{
		guard self < Self.range.upperBound else { return nil }
		return self.advanced(by: 1)
	}
}

extension Addressing where Self: FixedWidthInteger
{
	static var range: ClosedRange<Self>
	{
		return Self.min ... Self.max
	}
}

// Extend some common types so that they conform to `Addressing`
extension UInt16: Addressing {}
extension Int: Addressing {}

/// This is a protocol that must be adopted by a  type that represents code
protocol CodeRepresentation
{
	/// The type of an address in the code representation
	associatedtype Address: Addressing

	/// A list of all the errors encountered in the code.
	var errors: [DisassemblyError] { get }
}
/// Protocol adhered to by objects that can dissssemble code.
///
/// A parser can take a byte  stream and turn it into a sequence of lines.
///  `Code` is justa  placeholder for the type that the disassembler uses to
/// represent a disassembled program.
protocol Parser
{
	/// Type of source code produced by this disassembler
	associatedtype Code: CodeRepresentation

	/// Disassemble a program
	/// - Parameters:
	///   - program: The program to disassemble
	///   - info: info known about the program prior to parsing e.g. start address
	///           and labels.
	/// - Returns: The code of the  program
	/// - Throws: If the info has errors that prevent the codce from being
	///           generated
	func parse(program: ByteStream, info: [Info]) throws -> Code
}
/// Protocol adhered to by objects that can turn code into text streams.
///
/// `Code` is a placeholder forthe type of  the code  that can be handled. It
/// should match the type produced by a disassembler.
protocol CodeWriter
{
	/// Type of source code converted to text by this `CodeWriter`
	associatedtype Code: CodeRepresentation

	/// Write the code to a `TextOutputStream`
	/// - Parameters:
	///   - code: The  code to write
	///   - output: The output stream
	func write<Target: TextOutputStream>(_ code: Code, to output: inout Target)
}


/// A disassembler for a  particular type of machine code.
struct Disassembly<P: Parser, T: CodeWriter> where T.Code == P.Code
{
	let parser: P
	let codeWriter: T

	init(parser: P, codeWriter: T)
	{
		self.parser = parser
		self.codeWriter = codeWriter
	}

	var info: [Info] = []
	{
		didSet
		{
			if let program = program
			{
				parse(program: ByteStream(sequence: program), info: info)
			}
		}
	}

	var program: AnyRandomAccessCollection<UInt8>?
	{
		didSet
		{
			if let program = program
			{
				parse(program: ByteStream(sequence: program), info: info)
			}
		}
	}

	private mutating func  parse(program: ByteStream, info: [Info])
	{
		do
		{
			code = nil
			code = try parser.parse(program: program, info: info)
		}
		catch
		{
			reportError(error)
		}
	}

	var reportError: (Error) -> () = { log.error("\($0)") }

	private(set) var code: P.Code?
}

extension Disassembly: TextOutputStreamable
{
	func write<Target>(to target: inout Target) where Target : TextOutputStream
	{
		if let code = code
		{
			codeWriter.write(code, to: &target)
		}
	}
}

/// Impact of errors generated during disassembly
enum ErrorEffect
{
	/// Disassembly has to stop right away
	case fatal
	/// Disassembly can carry on butr no source can be created
	case stopsSourceGen
	/// Disassembly  can carry on but may produce incorrect source code
	case warning
}
protocol DisassemblyError: Swift.Error
{
	var effect: ErrorEffect { get }
}
