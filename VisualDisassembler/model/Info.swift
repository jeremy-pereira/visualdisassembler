//
//  Info.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 24/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Foundation
/// Configuration information about the disassembly
///
/// This gives us predefined labels, regions of various types etc
enum Info
{

	/// A label
	///
	/// - Parameters:
	///   - name: Label name
	///   - address: The address
	///   - isCode: Points into code. This is useful if the assembler wants
	///             to align code correctly. e.g. a 6502 code label can tell the
	///             assembler where an instructio starts.
	///   - comment: A comment to be inserted into the code with the label.
	case label(name: String, address: String, isCode: Bool, comment: String?)
	/// The one and only global information about the program
	///
	/// - Parameters:
	///   - startAddress: The start address of the program
	///   - cpu: The CPU of the cassembled code
	///   - outputFormat: The format of the disassembled output``
	case global(startAddress: String, cpu: String, outputFormat: String)
	/// A range in the code
	///
	/// - Parameters:
	///   - name: The name of the range (optional), if present there will be a
	///           label at the start.
	///   - start: The start address of the range
	///   - end: The end address of the range. The end address *is* included in
	///          the range.
	///   - type: The type of the range
	///   - comment: An optional comment for the range.
	case range(name: String?, start: String, end: String, type: String, comment: String?)

}

extension Info: Codable
{
	fileprivate enum CodingKeys: CodingKey
	{
		case label, global, range
	}

	fileprivate struct LabelEncoding: Codable
	{
		let name: String
		let address: String
		let isCode: Bool
		let comment: String?
	}

	fileprivate struct GlobalEncoding: Codable
	{
		let startAddress: String
		let cpu: String
		let outputFormat: String
	}

	fileprivate struct RangeEncoding: Codable
	{
		let name: String?
		let start: String
		let end: String
		let type: String
		let comment: String?
	}

	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		switch self
		{
		case .label(name: let name, address: let address, isCode: let isCode, comment: let comment):
			let values = LabelEncoding(name: name, address: address, isCode: isCode, comment: comment)
			try container.encode(values, forKey: .label)
		case .global(startAddress: let startAddress, cpu: let cpu, outputFormat: let outputFormat):
			let values = GlobalEncoding(startAddress: startAddress, cpu: cpu, outputFormat: outputFormat)
			try  container.encode(values, forKey: .global)
		case .range(name: let name, start: let start, end: let end, type: let type, comment: let comment):
			let values = RangeEncoding(name: name, start: start, end: end, type: type, comment: comment)
			try container.encode(values, forKey: .range)
		}
	}

	init(from decoder: Decoder) throws
	{
		let values = try decoder.container(keyedBy: CodingKeys.self)
		if values.contains(.global)
		{
			let value = try values.decode(GlobalEncoding.self, forKey: .global)
			self = .global(startAddress: value.startAddress, cpu: value.cpu, outputFormat: value.outputFormat)
		}
		else if values.contains(.label)
		{
			let value = try values.decode(LabelEncoding.self, forKey: .label)
			self = .label(name: value.name, address: value.address, isCode: value.isCode, comment: value.comment)
		}
		else if values.contains(.range)
		{
			let value = try values.decode(RangeEncoding.self, forKey: .range)
			self = .range(name: value.name, start: value.start, end: value.end, type: value.type, comment: value.comment)
		}
		else
		{
			throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Could not find a valid `Info` case"))
		}
	}
}

extension Info: Equatable
{
	
}
