//
//  M6502+Disassembler.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 28/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
import Toolbox

extension M6502
{
	struct Disassembler: Parser
	{
		/// Delegate for new symbol tables
		///
		/// This delegate should be compatible with the writer that will be used
		/// to write output e.g. so that symbol names are compatible with the
		/// output source format. The simplest thing is to make the writer itself
		/// a `SymbolTableDelegate` and assign it to this variable.
		var symbolTableDelegate: AnySymbolTableDelegete<UInt16>

		init<SD: SymbolTableDelegate>(symbolTableDelegate: SD)
			where SD.Address == UInt16
		{
			self.symbolTableDelegate = AnySymbolTableDelegete<UInt16>.wrap(symbolTableDelegate)
		}

		private static func singleByteInstruction(byte: UInt8, codeUnit: M6502.CodeUnit) -> CodeAction
		{
			CodeAction(Pattern(byteOrder: .byte,
						   elements: [Pattern.Element.Constant(UInt64(byte), bits: 0 ..< 8)]), { (_, _)  -> M6502.CodeUnit in return codeUnit })

		}
		private static func operandAInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b01, bits: 0 ..< 2),
				Pattern.Element.Field("addressMode", bits: 2 ..< 5),
				Pattern.Element.Field("operation", bits: 5 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				return M6502.CodeUnit.operandA(fields["operation"]!, M6502.CodeUnit.Operand.createOperand(number: fields["addressMode"]!, stream: stream, offset: 1))
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}

		private static func operandXInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b10, bits: 0 ..< 2),
				Pattern.Element.Field("addressMode", bits: 2 ..< 5),
				Pattern.Element.Field("operation", bits: 5 ..< 6),
				Pattern.Element.Constant(0b10, bits: 6 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				return M6502.CodeUnit.operandX(fields["operation"]!, M6502.CodeUnit.Operand.createOperandX(number: fields["addressMode"]!, stream: stream, offset: 1))
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}

		private static func operandYInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b00, bits: 0 ..< 2),
				Pattern.Element.Field("addressMode", bits: 2 ..< 5),
				Pattern.Element.Field("operation", bits: 5 ..< 7),
				Pattern.Element.Constant(0b1, bits: 7 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				return M6502.CodeUnit.operandY(fields["operation"]!, M6502.CodeUnit.Operand.createOperandY(number: fields["addressMode"]!, stream: stream, offset: 1))
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}

		private static func branchInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b1_0000, bits: 0 ..< 5),
				Pattern.Element.Field("isSet", bits: 5 ..< 6),
				Pattern.Element.Field("flag", bits: 6 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				let isSet = fields["isSet"]! == 1
				let flag = fields["flag"]!
				let operand: CodeUnit.Operand
				if let offset = stream[1]
				{
					operand = .relative(offset)
				}
				else
				{
					operand = .insufficientBytes
				}
				return M6502.CodeUnit.branch(flag, isSet, operand)
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}

		private static func incDecInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b110     , bits: 0 ..< 3),
				Pattern.Element.Field("addressMode", bits: 3 ..< 5),
				Pattern.Element.Field("incOrDec"   , bits: 5 ..< 6),
				Pattern.Element.Constant(0b011     , bits: 6 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				let isInc = fields["incOrDec"]! == 1
				let addressMode = fields["addressMode"]!
				let operand = M6502.CodeUnit.Operand.createOperandIncDec(number: addressMode, stream: stream, offset: 1)
				return isInc ? .inc(operand) : .dec(operand)
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}

		private static func jmpInstruction() -> CodeAction
		{
			let elements =
			[
			   Pattern.Element.Constant(0b01100   , bits: 0 ..< 5),
			   Pattern.Element.Field("addressMode", bits: 5 ..< 6),
			   Pattern.Element.Constant(0b01      , bits: 6 ..< 8),
			]
			let action  =
			{
			   (fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
			   let addressMode = fields["addressMode"]!
			   let operand = M6502.CodeUnit.Operand.createOperandJmp(number: addressMode, stream: stream, offset: 1)
			   return .jmp(operand)
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}


		private static func shiftRotateInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b110, bits: 0 ..< 3),
				Pattern.Element.Field("addressMode", bits: 3 ..< 5),
				Pattern.Element.Field("operation", bits: 5 ..< 7),
				Pattern.Element.Constant(0b0, bits: 7 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				return M6502.CodeUnit.shiftRotate(fields["operation"]!,
												  M6502.CodeUnit.Operand.createOperandIncDec(number: fields["addressMode"]!,
																							 stream: stream,
																							 offset: 1))
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}


		private static func bitInstruction() -> CodeAction
		{
			let elements =
			[
				Pattern.Element.Constant(0b100     , bits: 0 ..< 3),
				Pattern.Element.Field("addressMode", bits: 3 ..< 4),
				Pattern.Element.Constant(0b0010    , bits: 4 ..< 8),
			]
			let action  =
			{
				(fields: [String : UInt64], stream: ByteStream) -> M6502.CodeUnit in
				let addressMode = fields["addressMode"]!
				let operand = M6502.CodeUnit.Operand.createOperandBit(number: addressMode,
																	  stream: stream,
																	  offset: 1)
				return .bit(operand)
			}
			return CodeAction(Pattern(byteOrder: .byte, elements: elements), action)
		}

		private let actions: [Action] =
		[
			RangeAction(),
			Disassembler.singleByteInstruction(byte: 0b0000_0000, codeUnit: .brk),

			Disassembler.singleByteInstruction(byte: 0b0001_1000, codeUnit: .clc),
			Disassembler.singleByteInstruction(byte: 0b1101_1000, codeUnit: .cld),
			Disassembler.singleByteInstruction(byte: 0b0101_1000, codeUnit: .cli),
			Disassembler.singleByteInstruction(byte: 0b1011_1000, codeUnit: .clv),
			Disassembler.singleByteInstruction(byte: 0b0011_1000, codeUnit: .sec),
			Disassembler.singleByteInstruction(byte: 0b1111_1000, codeUnit: .sed),
			Disassembler.singleByteInstruction(byte: 0b0111_1000, codeUnit: .sei),

			Disassembler.singleByteInstruction(byte: 0b1100_1010, codeUnit: .dex),
			Disassembler.singleByteInstruction(byte: 0b1000_1000, codeUnit: .dey),
			Disassembler.singleByteInstruction(byte: 0b1110_1000, codeUnit: .inx),
			Disassembler.singleByteInstruction(byte: 0b1100_1000, codeUnit: .iny),
			Disassembler.singleByteInstruction(byte: 0b1110_1010, codeUnit: .nop),

			Disassembler.singleByteInstruction(byte: 0b0100_1000, codeUnit: .pha),
			Disassembler.singleByteInstruction(byte: 0b0000_1000, codeUnit: .php),
			Disassembler.singleByteInstruction(byte: 0b0110_1000, codeUnit: .pla),
			Disassembler.singleByteInstruction(byte: 0b0010_1000, codeUnit: .plp),

			Disassembler.singleByteInstruction(byte: 0b0000_1010, codeUnit: .asla),
			Disassembler.singleByteInstruction(byte: 0b0100_1010, codeUnit: .lsra),
			Disassembler.singleByteInstruction(byte: 0b0010_1010, codeUnit: .rola),
			Disassembler.singleByteInstruction(byte: 0b0110_1010, codeUnit: .rora),

			Disassembler.singleByteInstruction(byte: 0b0100_0000, codeUnit: .rti),
			Disassembler.singleByteInstruction(byte: 0b0110_0000, codeUnit: .rts),

			Disassembler.singleByteInstruction(byte: 0b1010_1010, codeUnit: .tax),
			Disassembler.singleByteInstruction(byte: 0b1010_1000, codeUnit: .tay),
			Disassembler.singleByteInstruction(byte: 0b1000_1010, codeUnit: .txa),
			Disassembler.singleByteInstruction(byte: 0b1001_1000, codeUnit: .tya),

			Disassembler.singleByteInstruction(byte: 0b1011_1010, codeUnit: .tsx),
			Disassembler.singleByteInstruction(byte: 0b1001_1010, codeUnit: .txs),

			Disassembler.bitInstruction(),

			Disassembler.jmpInstruction(),
			// JSR (operand number 3 is absolute)
			CodeAction(Pattern(byteOrder: .byte, elements: [Pattern.Element.Constant(0x20, bits: 0 ..< 8)]),
				{
					(fields, stream) in
					return .jsr(M6502.CodeUnit.Operand.createOperand(number: 3, stream: stream, offset: 1))
				}),

			Disassembler.branchInstruction(),

			Disassembler.incDecInstruction(),

			Disassembler.operandAInstruction(),
			Disassembler.operandXInstruction(),
			Disassembler.operandYInstruction(),

			Disassembler.shiftRotateInstruction(),

			CodeAction(Pattern(byteOrder: .byte, elements: [Pattern.Element.Field("byte", bits: 0 ..< 8)]),
				{
					(fields, _) in return .byte(UInt8(fields["byte"]!))
				}),


		]

		func parse(program: ByteStream, info: [Info]) throws -> M6502.Code
		{
			var symbols = SymbolTable<UInt16>(delegate: symbolTableDelegate)
			var ranges = RangeTable<UInt16, M6502.Code.ByteType>(defaultType: .code)
			let (startAddress, infoErrors) = try read(info: info,
													  symbols: &symbols,
													  ranges: &ranges)
			var code = Code(start: startAddress,
							predefinedSymbols: symbols,
							ranges: ranges)
			code.add(errors: infoErrors)

			var matchFound = true
			while matchFound
			{
				matchFound = false
				for action in actions
				{
					matchFound = action.tryToMatch(stream: program, code: &code)
					guard !matchFound else { break }
				}
			}
			return code
		}

		private func read(info: [Info],
						  symbols: inout SymbolTable<UInt16>,
						  ranges: inout RangeTable<UInt16, M6502.Code.ByteType>) throws -> (UInt16, [DisassemblyError])
		{
			var potentialStartAddress: UInt16?
			var infoErrors: [DisassemblyError] = []
			try info.forEach
			{
				do
				{
					switch $0
					{
					case .global(startAddress: let startAddressString, _, outputFormat: _):
						guard potentialStartAddress == nil
							else { throw M6502.Error.duplicateGlobal }
						potentialStartAddress = try symbols.addressFrom(info: startAddressString)
					case .label(name: let name, address: let addressString, isCode: let isCode, comment: let comment):
						try symbols.addFromInfo(name: name, address: addressString, isCode: isCode, comment: comment)
					case .range(name: let name, start: let startString, end: let endString, type: let typeString, comment: let comment):
						let start = try symbols.addressFrom(info: startString)
						let end = try symbols.addressFrom(info: endString)
						let type = try decode(typeString: typeString)
						try ranges.insert(range: try ClosedRange(startAddress: start, endAddress: end),
										  type: type)
						if let name = name
						{
							// TODO: Eliminate double conversion of start address
							try symbols.addFromInfo(name: name, address: startString, isCode: type == .code, comment: comment)
						}
						else
						{
							symbols.autoCreate(address: start, isDefinitelyCode: type == .code, comment: comment)
						}
					}
				}
				catch
				{
					guard let infoError = error as? DisassemblyError, infoError.effect == .warning
						else { throw error }
					infoErrors.append(infoError)
				}
			}
			guard let startAddress = potentialStartAddress else { throw M6502.Error.noStartAddress }
			return (startAddress, infoErrors)
		}

		private func decode(typeString: String) throws -> M6502.Code.ByteType
		{
			switch typeString.lowercased()
			{
			case "code":
				return .code
			case "byte":
				return .byte
			case  "word":
				return .word
			default:
				throw M6502.Error.invalidDataType("")
			}
		}
	}
}

fileprivate extension M6502.Disassembler
{
	class Action
	{

		init() {}

		/// Try to match the next byte(s) in the stream
		///
		/// - Parameters:
		///   - stream: The program stream to match
		///   - code: Code that will be amended by the action
		/// - Returns: True  if  the match succeeded
		func tryToMatch(stream: ByteStream, code: inout M6502.Code) -> Bool
		{
			mustBeOverridden(self)
		}
	}

	class CodeAction: Action
	{
		init(_ pattern: Pattern, _ action: @escaping ([String : UInt64], ByteStream) -> M6502.CodeUnit)
		{
			self.pattern = pattern
			self.action = action
			super.init()
		}

		let action: ([String : UInt64], ByteStream) -> M6502.CodeUnit
		private let pattern: Pattern

		/// Try to match  the  pattern to the next byte(s) in the stream
		///
		/// We first see if the pattern matches the beginning of the stream.
		/// Then, if it does, we generate the code unit. Then we check if
		/// there is a code label pointing at any of the bytes except the first
		/// one and, if there is we reject the pattern because it would mean
		/// jumping in to the middle of an instruction.
		/// - Parameter code: Code that will be amended by the action
		/// - Returns: True  if  the match succeeded
		override func tryToMatch(stream: ByteStream, code: inout M6502.Code) -> Bool
		{
			guard let fields = pattern.match(stream: stream, consumeOnMatch: false) else { return false }
			let codeUnit = action(fields, stream)
			if codeUnit.byteCount > 1
			{
				let unbreakableRange = (code.end + 1) ... (code.end + UInt16(codeUnit.byteCount - 1))
				let midInstructionSymbols = code.symbols[unbreakableRange]
				if midInstructionSymbols.first(where: { $0.isDefinitelyCode }) != nil
				{
					return false
				}
			}
			code.append(codeUnit)
			stream.discard(count: codeUnit.byteCount)
			return true
		}
	}

	class RangeAction: Action
	{
		override func tryToMatch(stream: ByteStream, code: inout M6502.Code) -> Bool
		{
			let range = code.ranges.range(of: code.end)
			precondition(range.byteCount > 0, "Asumption that range is not empty is false")
			var bytesNeeded = range.byteCount
			switch range.type
			{

			case .code:
				return false
			case .byte:
				var theBytes: [UInt8] = []
				while bytesNeeded > 0, let byte = stream.next()
				{
					theBytes.append(byte)
					bytesNeeded -= 1
				}
				code.append(.byteData(theBytes))
			case .word:
				var theWords: [UInt16] = []
				while bytesNeeded >= 2, let word = ByteOrder.littleEndian16.uint16(stream: stream, offset: 0)
				{
					theWords.append(word)
					stream.discard(count: UInt16.bitWidth / UInt8.bitWidth)
					bytesNeeded -= 2
				}
				code.append(.wordData(theWords))
				// If the range had an odd size, consume the last byte in the range
				if bytesNeeded == 1
				{
					if let byte = stream.next()
					{
						code.append(.byte(byte))
					}
				}
			}
			return true
		}
	}
}
