//
//  Pattern.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 19/03/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

/// A pattern to match
///
/// A pattern has a sequence of elements to match. Each element matches against
/// bits from the byte stream that is read in chunks according to the byte
/// order.
struct Pattern
{
	/// Elements in the pattern
	///
	/// Each element in the pattern is either a constant of a variable field and
	/// each has a bit width.
	///
	let elements: [Element]

	let byteOrder: ByteOrder


	init(byteOrder: ByteOrder, elements: [Element])
	{
		guard byteOrder.count * UInt8.bitWidth < UInt64.bitWidth
			else { fatalError("We do not currently support more than 64 bits") }
		self.elements = elements
		self.byteOrder = byteOrder
	}

	/// Match the start of the stream to this pattern
	/// - Parameters:
	///   - stream: The stream to match
	///   - offset: The offset in the stream to start matching at. Default is 0
	///   - consumeOnMatch: True if you want to consume the stream on a match.
	///                     Defaults to `true`
	/// - Returns: `nil` for no match, a dictionary of the fields found if there
	///            was a match.
	func match(stream: ByteStream, offset: Int = 0, consumeOnMatch: Bool = true) -> [String : UInt64]?
	{
		// If we don't have enough bytes to match, no match
		guard let bits = byteOrder.uint64(stream: stream, offset: offset) else { return nil }

		let (elementMatches, didFinish) = elements.reduceWithStop(into: [])
		{
			(accumulated: inout [Match], element, stop) in
			let matchResult = element.match(bits: bits)
			switch matchResult
			{
			case .none:
				stop = true
			default:
				accumulated.append(matchResult)
			}
		}
		guard didFinish else { return nil }
		if consumeOnMatch
		{
			stream.discard(count: byteOrder.count)
		}
		let fields: [String : UInt64] = elementMatches.reduce(into: [:])
		{
			(fields, match) in
			if case Match.field(let label, let bits) = match
			{
				fields[label] = bits
			}
		}
		return fields
	}
}

extension Pattern
{
	class Element
	{
		let bitRange: Range<Int>
		fileprivate init(bits: Range<Int>)
		{
			guard bits.lowerBound >= 0 && bits.upperBound <= UInt64.bitWidth
				else { fatalError("Bit range for pattern element is outside that of a `UInt64`") }
			self.bitRange = bits
		}

		fileprivate func match(bits: UInt64) -> Pattern.Match
		{
			mustBeOverridden(self)
		}

		fileprivate var maskOut: UInt64
		{
			let topBits = bitRange.upperBound < UInt64.bitWidth
							? ~UInt64(0) << bitRange.upperBound : UInt64(0)
			let bottomBits = ~(~UInt64(0) << bitRange.lowerBound)
			return topBits | bottomBits
		}
		fileprivate var maskIn: UInt64 { ~maskOut }
	}
}

extension Pattern
{
	enum Match
	{
		case none
		case constant
		case field(String, UInt64)
	}
}

extension Pattern.Element
{
	class Constant: Pattern.Element
	{
		let value: UInt64

		init(_ value: UInt64, bits: Range<Int>)
		{
			self.value = value
			super.init(bits: bits)
		}

		override func match(bits: UInt64) -> Pattern.Match
		{
			let theMask = maskIn
			let maskedBits = (bits & theMask) >> bitRange.lowerBound
			return  maskedBits == value ? .constant : .none
		}
	}

	class Field: Pattern.Element
	{
		let label: String

		init(_ label: String, bits: Range<Int>)
		{
			self.label = label
			super.init(bits: bits)
		}

		override func match(bits: UInt64) -> Pattern.Match
		{
			let theMask = maskIn
			let maskedBits = (bits & theMask) >> bitRange.lowerBound
			return  .field(label, maskedBits)
		}
	}
}
