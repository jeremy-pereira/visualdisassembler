//
//  M6502+CA65Writer.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 17/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import Toolbox

extension M6502
{
	struct CA65Writer: CodeWriter
	{
		func write<Target>(_ code: M6502.Code, to output: inout Target)
			where Target : TextOutputStream
		{
			var address = code.start
			code.units.forEach
			{
				code.symbols[address].forEach
				{
					write(symbol: $0, to: &output)
				}
				write(codeUnit: $0, address: address, to: &output)
				address = address &+ UInt16($0.byteCount)
			}
			list(symbols: code.symbols, to: &output)
			list(errors: code.errors, to: &output)
		}

		func write<Target: TextOutputStream>(symbol: Symbol<UInt16>, to output: inout Target)
		{
			if let comment = symbol.comment
			{
				if !symbol.autoCreated || comment.contains("\n")
				{
					output.write(";" + String(repeating: "-", count: 79) + "\n")
					let commentLines = comment.split(separator: "\n")
					commentLines.forEach
					{
						output.write("; \($0)\n")
					}
					output.write("\(symbol.name):\n")
				}
				else
				{
					output.write("\(symbol.name): ; \(comment)\n")
				}
			}
			else
			{
				output.write("\(symbol.name):\n")
			}
		}

		static let readOperations = ["ora", "and", "eor", "adc", "sta", "lda", "cmp", "sbc"]
		static let xOperations = ["stx", "ldx"]
		static let yOperations = ["sty", "ldy", "cpy", "cpx"]
		static let shiftRotateOperations = ["asl", "rol", "lsr", "ror"]

		func write<Target: TextOutputStream>(codeUnit: M6502.CodeUnit, address: UInt16, to target: inout Target)
		{
			switch codeUnit
			{
			case .brk, .clc, .cld, .cli, .clv, .dex, .dey, .inx, .iny, .nop,
				 .pha, .php, .pla, .plp, .rti, .rts, .sec, .sed, .sei, .tax,
				 .tay, .txa, .tya, .tsx, .txs:
				writeCodeLine(mnemonic: codeUnit.mnemonic, address: address, target: &target)
			case .asla, .lsra, .rola, .rora:
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: "a", address: address, target: &target)
			case .byte(let byte):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: "$\(byte.hexString)", address: address, target: &target)
			case .operandA(_, let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .operandX(_, let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .operandY(_, let operand):
					writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .branch(_, _, let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .inc(let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .dec(let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .jsr(let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .jmp(let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .shiftRotate(_, let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .bit(let operand):
				writeCodeLine(mnemonic: codeUnit.mnemonic, operand: stringify(operand: operand), address: address, target: &target)
			case .error(let error):
				target.write("; ERROR: \(error)\n")
			case .byteData(let bytes):
				writeHexData(prefix: ".byte", address: address, numbers: bytes, target: &target)
			case .wordData(let words):
				writeHexData(prefix: ".word", address: address, numbers: words, target: &target)
			}
		}

		private func writeHexData<N: FixedWidthInteger, T: TextOutputStream>(prefix: String, address: UInt16, numbers: [N], target: inout T)
		{
			let numbersPerLine = max(8 / (N.bitWidth / UInt8.bitWidth), 1)
			let numberSlices = numbers.slices(ofSize: numbersPerLine)
			let stringSlices = numberSlices.map { hexList(sequence: $0) }
			stringSlices.enumerated().forEach
			{
				let (index, operand) = $0
				writeCodeLine(mnemonic: prefix,
							  operand: operand,
							  address: address &+ UInt16(index * numbersPerLine),
							  target: &target)
			}
		}

		private func hexList<S: Sequence>(sequence: S) -> String
			where S.Element: FixedWidthInteger
		{
			let digits = 2 * S.Element.bitWidth / UInt8.bitWidth
			let strings = sequence.map { String(format: "$%0*x", digits, Int($0)) }
			return strings.joined(separator: ",")
		}

		private let tabWidth = 8
		private let labelTab = 0
		private let mnemonicTab = 1
		private let operandTab = 2
		private let commentTab = 6

		private func writeCodeLine<T: TextOutputStream>(label: String? = nil,
														mnemonic: String,
														operand: String? = nil,
														address: UInt16,
														lineFeed: Bool = true,
														target: inout T)
		{
			let labelWidth = (mnemonicTab - labelTab) * tabWidth

			var stringToWrite: String = ""
			if let label = label
			{
				let labelWithColon  = label + ":"
				if labelWithColon.count + 1 < labelWidth // +1 for a space
				{
					stringToWrite += "\(label):"
				}
				else
				{
					target.write("\(labelWithColon)\n")
				}
			}
			stringToWrite.addSpaces(to: mnemonicTab * tabWidth)
			stringToWrite.append(mnemonic)
			stringToWrite.addSpaces(to: operandTab * tabWidth)
			stringToWrite.append(operand ?? "")
			stringToWrite.addSpaces(to: commentTab * tabWidth)
			stringToWrite.append("; \(address.hexString)")
			if lineFeed
			{
				stringToWrite.append("\n")
			}
			target.write(stringToWrite)
		}

		private func stringify(operand: M6502.CodeUnit.Operand) -> String
		{
			let operandString: String
			switch operand
			{
			case .zpXIndirect(let address):
				operandString = "($\(address.hexString),x)"
			case .zp(let address):
				operandString = "$\(address.hexString)"
			case .immediate(let constant):
				operandString = "#$\(constant.hexString)"
			case .absolute(let address):
				operandString = "$\(address.hexString)"
			case .absoluteIndirect(let address):
				operandString = "($\(address.hexString))"
			case .zpIndirectY(let address):
				operandString = "($\(address.hexString)),y"
			case .zpX(let address):
				operandString = "$\(address.hexString),x"
			case .zpY(let address):
				operandString = "$\(address.hexString),y"
			case .absoluteX(let address):
				operandString = "$\(address.hexString),x"
			case .absoluteY(let address):
				operandString = "$\(address.hexString),y"
			case .relative(let offset):
				operandString = "$\(offset.hexString)"
			case .insufficientBytes:
				operandString = "NO BYTES"
			case .unknown:
				operandString = "???"
			}
			return operandString
		}

		private func list<Target: TextOutputStream>(symbols: SymbolTable<UInt16>, to output: inout Target)
		{
			let symbolList = symbols.allSymbols.sorted { $0.name < $1.name }

			for symbol in symbolList
			{
				output.write("; \(stringify(symbol: symbol))\n")
			}
		}

		private func stringify(symbol: Symbol<UInt16>) -> String
		{
			return "\(symbol.name)\t \(symbol.address.hexString)\t \(symbol.isDefinitelyCode ? "code" : "data?")\t \(symbol.comment ?? "")"
		}

		private func list<Errors: Sequence, Target: TextOutputStream>(errors: Errors, to output: inout Target)
		where Errors.Element == DisassemblyError
		{
			for error in errors
			{
				output.write("\(error)\n")
			}
		}
	}
}

extension M6502.CA65Writer: SymbolTableDelegate
{

	func stringifyForLabel(address: UInt16) -> String
	{
		return "L\(address.hexString)"
	}

	func uniquify(_ name: String) -> String
	{
		guard let underscoreIndex = name.lastIndex(of: "_") else { return name + "_1" }
		let suffixStart = name.index(after: underscoreIndex)
		guard suffixStart != name.endIndex else { return name + "_1" }
		let suffix = name[suffixStart ..< name.endIndex]
		guard let suffixNumeric = Int(suffix) else { return name + "_1" }
		return name + String(suffixNumeric + 1)
	}

	func addressFrom(info: String) throws -> UInt16
	{
		if info.hasPrefix("$")
		{
			guard let ret = UInt16(info.dropFirst(), radix: 16)
				else { throw Symbol<UInt16>.Error.invalidAddress(info) }
			return ret
		}
		else if info.hasPrefix("0x")
		{
			guard let ret = UInt16(info.dropFirst().dropFirst(), radix: 16)
				else { throw Symbol<UInt16>.Error.invalidAddress(info) }
			return ret
		}
		else
		{
			guard let ret = UInt16(info.dropFirst().dropFirst(), radix: 10)
				else { throw Symbol<UInt16>.Error.invalidAddress(info) }
			return ret
		}
	}
}

fileprivate extension M6502.CodeUnit
{
	var mnemonic: String
	{
		switch self
		{
		case .asla:
			return "asl"
		case .brk:
			return "brk"
		case .clc:
			return "clc"
		case .cld:
			return "cld"
		case .cli:
			return "cli"
		case .clv:
			return "clv"
		case .dex:
			return "dex"
		case .dey:
			return "dey"
		case .inx:
			return "inx"
		case .iny:
			return "iny"
		case .lsra:
			return "lsr"
		case .nop:
			return "nop"
		case .pha:
			return "pha"
		case .php:
			return "php"
		case .pla:
			return "pla"
		case .plp:
			return "plp"
		case .rola:
			return "rol"
		case .rora:
			return "ror"
		case .rti:
			return "rti"
		case .rts:
			return "rts"
		case .sec:
			return "sec"
		case .sed:
			return "sed"
		case .sei:
			return "sei"
		case .tax:
			return "tax"
		case .tay:
			return "tay"
		case .tsx:
			return "tsx"
		case .txa:
			return "txa"
		case .txs:
			return "txs"
		case .tya:
			return "tya"
		case .operandA(let operation, _):
			return M6502.CA65Writer.readOperations[Int(operation)]
		case .operandX(let operation, _):
			return M6502.CA65Writer.xOperations[Int(operation)]
		case .operandY(let operation, _):
			return M6502.CA65Writer.yOperations[Int(operation)]
		case .branch(let flag, let isSet, _):
			switch (flag, isSet)
			{
			case (0, true): return "bmi"
			case (0, false): return "bpl"
			case (1, _): return "bv" + (isSet ? "s" : "c")
			case (2, _): return "bc" + (isSet ? "s" : "c")
			case (3, true): return "beq"
			case (3, false): return "bne"
			default:
				return "???"
			}
		case .byte(_):
			return ".byte"
		case .inc(_):
			return "inc"
		case .dec(_):
			return "dec"
		case .jsr(_):
			return "jsr"
		case .jmp(_):
			return "jmp"
		case .shiftRotate(let operation, _):
			return M6502.CA65Writer.shiftRotateOperations[Int(operation)]
		case .bit(_):
			return "bit"
		case .byteData(_):
			return ".byte"
		case .wordData(_):
			return ".word"
		case .error(_):
			return "error"
		}
	}
}

fileprivate extension String
{
	mutating func addSpaces(to tabStop: Int)
	{
		let spacesShort = max(tabStop - self.count, 1) // MAke sure of at least one space
		let filler = String(repeating: " ", count: spacesShort)
		self.append(filler)
	}
}

fileprivate extension Array
{
	func slices(ofSize sliceSize: Int) -> [ArraySlice<Element>]
	{
		let sliceCount = (self.count + sliceSize - 1) / sliceSize
		let slices = (0 ..< sliceCount)
			.map { $0 * sliceSize ..< Swift.min(($0 + 1) * sliceSize, self.count) }
			.map { self[$0] }
		return slices
	}
}
