//
//  M6502+CodeUnit.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 17/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

extension M6502
{
	/// Represents a "line" of code
	enum CodeUnit
	{
		enum Operand
		{
			case zpXIndirect(UInt8)
			case zp(UInt8)
			case immediate(UInt8)
			case absolute(UInt16)
			case zpIndirectY(UInt8)
			case zpX(UInt8)
			case zpY(UInt8)
			case absoluteX(UInt16)
			case absoluteY(UInt16)
			case absoluteIndirect(UInt16)
			case relative(UInt8)
			case insufficientBytes
			case unknown

			var byteCount: Int
			{
				switch self
				{
				case .zpXIndirect,  .zp, .immediate, .zpIndirectY, .zpX, .zpY,
					 .relative:
					return 1
				case .absolute, .absoluteX, .absoluteY, .absoluteIndirect:
					return 2
				case .unknown, .insufficientBytes:
					return 0
				}
			}

			var address: UInt16?
			{
				switch self
				{
				case .zpXIndirect(let address):
					return UInt16(address)
				case .zp(let address):
					return UInt16(address)
				case .immediate:
					return nil
				case .absolute(let address):
					return address
				case .zpIndirectY(let address):
					return UInt16(address)
				case .zpX(let address):
					return UInt16(address)
				case .zpY(let address):
					return UInt16(address)
				case .absoluteX(let address):
					return address
				case .absoluteY(let address):
					return  address
				case .absoluteIndirect(let address):
					return address
				case .relative:
					return nil
				case .insufficientBytes, .unknown:
					return nil
				}
			}

			static func createOperand(number: UInt64, stream: ByteStream, offset: Int) -> Operand
			{
				let ret: Operand
				switch number
				{
				case 0:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zpXIndirect(address)
				case 1:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zp(address)
				case 2:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .immediate(address)
				case 3:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absolute(address)
				case 4:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zpIndirectY(address)
				case 5:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zpX(address)
				case 6:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absoluteX(UInt16(address))
				case 7:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absoluteY(address)
				default:
					ret = .unknown
				}
				return ret
			}

			static func createOperandX(number: UInt64, stream: ByteStream, offset: Int) -> Operand
			{
				let ret: Operand
				switch number
				{
				case 0:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .immediate(address)
				case 1:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zp(address)
				case 3:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absolute(address)
				case 5:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zpY(address)
				case 7:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absoluteY(address)
				default:
					ret = .unknown
				}
				return ret
			}

			static func createOperandY(number: UInt64, stream: ByteStream, offset: Int) -> Operand
			{
				let ret: Operand
				switch number
				{
				case 0:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .immediate(address)
				case 1:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zp(address)
				case 3:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absolute(address)
				case 5:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zpX(address)
				case 7:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absoluteX(address)
				default:
					ret = .unknown
				}
				return ret
			}

			static func createOperandIncDec(number: UInt64, stream: ByteStream, offset: Int) -> Operand
			{
				let ret: Operand
				switch number
				{
				case 0:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zp(address)
				case 1:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absolute(address)
				case 2:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zpX(address)
				case 3:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
						else { return .insufficientBytes }
					ret = .absoluteX(address)
				default:
					return .unknown
				}
				return ret
			}

			static func createOperandJmp(number: UInt64, stream: ByteStream, offset: Int) -> Operand
			{
				guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
				else { return .insufficientBytes }

				switch number
				{
				case 0:
					return .absolute(address)
				case 1:
					return .absoluteIndirect(address)
				default:
					return .unknown
				}
			}

			static func createOperandBit(number: UInt64, stream: ByteStream, offset: Int) -> Operand
			{
				let ret: Operand
				switch number
				{
				case 0:
					guard let address = stream[offset] else { return .insufficientBytes }
					ret = .zp(address)
				case 1:
					guard let address = ByteOrder.littleEndian16.uint16(stream: stream, offset: offset)
					else { return .insufficientBytes }
					ret = .absolute(address)
				default:
					ret = .unknown
				}
				return ret
			}
		}

		case asla
		case brk
		case clc
		case cld
		case cli
		case clv
		case dex
		case dey
		case inx
		case iny
		case lsra
		case nop
		case pha
		case php
		case pla
		case plp
		case rola
		case rora
		case rti
		case rts
		case sec
		case sed
		case sei
		case tax
		case tay
		case tsx
		case txa
		case txs
		case tya
		/// Operation on an operand and the accumulator
		///
		/// - Parameters:
		///   - `UInt64` operation
		///   = `Operand` The operand
		case operandA(UInt64, Operand)
			/// Operation on an operand and the X index
			///
			/// - Parameters:
			///   - `UInt64` operation
			///   = `Operand` The operand
		case operandX(UInt64, Operand)
			/// Operation on an operand and the Y index
			///
			/// - Parameters:
			///   - `UInt64` operation
			///   = `Operand` The operand
		case operandY(UInt64, Operand)
		/// A branch instruction
		///
		/// - Parameters:
		///   - `UInt64` the flag
		///   - `Bool` test is flag is set
		///   - `UInt8` the branch offset
		case branch(UInt64, Bool, Operand)
		/// Byte that can't be disassembled
		case byte(UInt8)
		/// Increment an operand
		case inc(Operand)
		/// Decrement an operand
		case dec(Operand)
		/// Jump saving return
		///
		/// Note that, in practice, operand is always an absolute address
		case jsr(Operand)
		/// Jump
		case jmp(Operand)
		/// Shift or rotate the operand
		case shiftRotate(UInt64, Operand)
		/// Bit test against accumulator
		case bit(Operand)
		/// Byte data from a `.byte` range
		case byteData([UInt8])
		/// Data from a `.word` range
		case wordData([UInt16])

		/// An error encountered when parsing the code
		case error(Swift.Error)

		var operand: Operand?
		{
			switch self
			{
			case .asla, .brk, .clc, .cld, .cli, .clv, .dex, .dey, .inx, .iny,
				 .lsra, .nop, .pha, .php, .pla, .plp, .rola, .rora, .rti, .rts,
				 .sec, .sed, .sei, .tax, .tay, .tsx, .txa, .txs, .tya, .byte,
				 .byteData, .wordData, .error:
				return nil
			case .operandA(_, let operand):
				return operand
			case .operandX(_, let operand):
				return operand
			case .operandY(_, let operand):
				return operand
			case .branch(_, _, let operand):
				return operand
			case .inc(let operand):
				return operand
			case .dec(let operand):
				return operand
			case .jsr(let operand):
				return operand
			case .jmp(let operand):
				return operand
			case .shiftRotate(_, let operand):
				return operand
			case .bit(let operand):
				return operand
			}
		}

		var byteCount: Int
		{
			switch self
			{
			case .byteData(let bytes):
				return bytes.count
			case .wordData(let words):
				return words.count * (UInt16.bitWidth / UInt8.bitWidth)
			default:
				return 1 + (self.operand?.byteCount ?? 0)
			}
		}

		var address: UInt16?
		{
			return self.operand?.address
		}
	}

}
