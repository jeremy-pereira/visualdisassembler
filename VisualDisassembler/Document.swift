//
//  Document.swift
//  VisualDisassembler
//
//  Created by Jeremy Pereira on 18/03/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


import Cocoa
import Toolbox

private let log = Logger.getLogger("VisualDisassembler.Document")

class Document: NSDocument
{
	@IBOutlet weak var programPathDisplay: NSPathControl!

	// TODO: Get rid of this once open/save working and we can add info
	private static var documentInfo: [Info] =
	[
		.global(startAddress: "$03ff", cpu: "6502", outputFormat: "ca65"),
		.label(name: "showSoundWiring", address: "$0412", isCode: true, comment: "Shoews a screen with the wiring diagram"),
		.range(name: "loadAddress", start: "$03ff", end: "$0400", type: "Byte", comment: "Basic load address"),
		.range(name: "basicBootstrap", start: "$0401", end: "$040e", type: "Byte", comment: "Basic bootstrap code"),
		.label(name: "entryPoint", address: "$040f", isCode: true, comment: "Machine code starts here"),
	]

	override init()
	{
		info = DisassemblyInfo()
	    super.init()
	}

	override class var autosavesInPlace: Bool { true }

	override var windowNibName: NSNib.Name?
	{
		return NSNib.Name("Document")
	}

	override func windowControllerDidLoadNib(_ windowController: NSWindowController)
	{
		programPathDisplay.url = info.programUrl
	}

	override func data(ofType typeName: String) throws -> Data
	{
		log.debug("Encoding document of type \(typeName)")
		let encoder = JSONEncoder()
		encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
		return try encoder.encode(info)
	}

	override func read(from data: Data, ofType typeName: String) throws
	{
		log.debug("Reading data of type \(typeName)")
		if log.isDebug, let dataAsString = String(bytes: data, encoding: .utf8)
		{
			log.debug("\(dataAsString)")
		}
		// Insert code here to read your document from the given data of the
		// specified type, throwing an error in case of failure.
		// Alternatively, you could remove this method and override
		// read(from:ofType:) instead.
		// If you do, you should also override isEntireFileLoaded to return
		// false if the contents are lazily loaded.
		let decoder = JSONDecoder()
		info = try decoder.decode(DisassemblyInfo.self, from: data)
		log.debug("We have \(undoManager == nil ? "no" : "an") undo manager")
		self.undoManager?.removeAllActions()
		queueDisassembly()
	}

	private func queueDisassembly()
	{
		DispatchQueue.main.async
		{
			[weak self] in
			guard let strongSelf = self else { return }
			strongSelf.programPathDisplay.url = strongSelf.info.programUrl
			if strongSelf.info.programUrl != nil
			{
				do
				{
					let target = try strongSelf.makeDisassemblyFileUrl()
					try strongSelf.info.executeWithProgramUrl(documentUrl: strongSelf.fileURL)
					{
						try strongSelf.disassemble(from: $0, to: target)
					}
					strongSelf.outputURL = target
				}
				catch
				{
					strongSelf.presentError(error)
				}
			}
		}
	}

	private var info: DisassemblyInfo

	private var outputURL: URL? = nil

	@IBAction func specifyProgram(sender: Any)
	{
		log.debug("Locating program")
		let openPanel = NSOpenPanel()
		openPanel.canChooseFiles = true
		openPanel.canChooseDirectories = false
		openPanel.allowsMultipleSelection = false

		openPanel.begin
		{
			if $0 == .OK
			{
				guard let url = openPanel.url else { return }
				do
				{
					let oldInfo = self.info
					self.undoManager?.registerUndo(withTarget: self,
												   handler: { $0.info = oldInfo })
					try self.info.set(programUrl: url, documentUrl: self.fileURL)
					self.queueDisassembly()
				}
				catch
				{
					self.presentError(error)
				}
			}
		}
	}

	private func makeDisassemblyFileUrl() throws -> URL
	{
		let appSupportDirectories = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask)
		// Making the assumption that we always have one.
		guard let myAppSupportDirectory = appSupportDirectories.first
		else
		{
			throw Error.cannotCreateDisassemblySource("Cannnot locate user's application support directory")
		}
		let myBundleIdentifier = Bundle.main.bundleIdentifier ?? "net.jeremyp.visualdisassembler"
		let myBundleAppSupportDirectory = myAppSupportDirectory.appendingPathComponent(myBundleIdentifier)
		try FileManager.default.createDirectory(at: myBundleAppSupportDirectory, withIntermediateDirectories: true, attributes: nil)
		let sourceFile = myBundleAppSupportDirectory.appendingPathComponent("source.s")
		log.debug("Assembly source file will be \(sourceFile)")
		return sourceFile
	}

	private func disassemble(from: URL, to: URL) throws
	{
		let inputStream = try FileInStream(url: from)
		let data = try inputStream.readAllBytes()
		var disassembly = M6502.makeCA65Disassembler()
		disassembly.program = AnyRandomAccessCollection(data)
		guard var outStream = OutputStream(url: to, append: false)
		else
		{
			throw Error.cannotCreateOutput(to)
		}
		outStream.open()
		if let error = outStream.streamError
		{
			throw error
		}
		defer { outStream.close() }
		disassembly.write(to: &outStream)
		if let error = outStream.streamError
		{
			throw error
		}
	}
}

extension Document
{
	enum Error: Swift.Error
	{
		case cannotCreateDisassemblySource(String)
		case cannotCreateOutput(URL)
		case infoHasNoBookmark
		case infoHasNoUrl
		case bookmarkInvalid
		case bookmarkIsStale
	}
}

extension  OutputStream: TextOutputStream
{
	public func write(_ string: String)
	{
		guard string.count != 0 else { return } // Need this to ensure buffer
		                                        // below is not empty and does
		                                        // have a base address
		guard let data = string.data(using: .utf8) else { fatalError("Cannot encode string as UTF8") }
		data.withUnsafeBytes
		{
			(b: UnsafeRawBufferPointer) in
			let boundBuffer = b.bindMemory(to: UInt8.self)
			self.write(boundBuffer.baseAddress!, maxLength: data.count)
		}
	}
}
