//
//  ByteOrderTests.swift
//  VisualDisassemblerTests
//
//  Created by Jeremy Pereira on 01/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import VisualDisassembler

class ByteOrderTests: XCTestCase
{

    func testLE16() throws
	{
		let testStream = ByteStream(sequence: [1, 2, 3, 4])

		let le16Bytes = ByteOrder.littleEndian16.bytes(stream: testStream, offset: 0)
		XCTAssert(le16Bytes == [1, 2])
		let le16UInt64 = ByteOrder.littleEndian16.uint64(stream: testStream, offset: 0)
		XCTAssert(le16UInt64 == 0x0201)
    }


}
