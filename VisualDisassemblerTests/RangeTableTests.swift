//
//  RangeTableTests.swift
//  VisualDisassemblerTests
//
//  Created by Jeremy Pereira on 28/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import VisualDisassembler

class RangeTableTests: XCTestCase
{

    func testBasicRange()
	{
		var rangeTable = RangeTable<UInt16, M6502.Code.ByteType>(defaultType: .code)
		do
		{
			try rangeTable.insert(range: 0x100 ... 0x1ff, type: .byte)
			try rangeTable.insert(range: 0xfffa ... 0xffff, type: .word)
			try rangeTable.insert(range: 0x300 ... 0x3ff, type: .byte)
			try rangeTable.insert(range: 0x400 ... 0x4ff, type: .code)
			try rangeTable.insert(range: 0x00 ... 0xff, type: .word)
			let stackRange = rangeTable.range(of: 0x100)
			XCTAssert(stackRange.byteCount == 0x100)
			XCTAssert(stackRange.range == 0x100 ... 0x1ff)
			XCTAssert(stackRange.type == .byte)
			let zpRange = rangeTable.range(of: 0x0f)
			XCTAssert(zpRange.byteCount == 0x100)
			XCTAssert(zpRange.range == 0x00 ... 0xff)
			XCTAssert(zpRange.type == .word)
			let unusedRange = rangeTable.range(of: 0x1001)
			XCTAssert(unusedRange.byteCount == 0xfafa, "Incorrect range size: \(unusedRange.byteCount)")
			XCTAssert(unusedRange.range == 0x500 ... 0xfff9)
			XCTAssert(unusedRange.type == .code)
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

	func testNoRangeAtEnd()
	{
		var rangeTable = RangeTable<UInt16, M6502.Code.ByteType>(defaultType: .code)
		do
		{
			try rangeTable.insert(range: 0x100 ... 0x1ff, type: .byte)
			let zpRange = rangeTable.range(of: 0x0f)
			XCTAssert(zpRange.byteCount == 0x100)
			XCTAssert(zpRange.range == 0x00 ... 0xff)
			XCTAssert(zpRange.type == .code)
			let unusedRange = rangeTable.range(of: 0x1001)
			XCTAssert(unusedRange.byteCount == 0xfe00, "Incorrect range size: \(unusedRange.byteCount)")
			XCTAssert(unusedRange.range == 0x200 ... 0xffff)
			XCTAssert(unusedRange.type == .code)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testBrokenRange()
	{
		do
		{
			let _ = try ClosedRange(startAddress: 0xff, endAddress: 0x00)
		}
		catch
		{
			switch error
			{
			case ByteRangeError.invalidDelimiters:
				break
			default:
				XCTFail("\(error)")
			}
		}
	}
}
