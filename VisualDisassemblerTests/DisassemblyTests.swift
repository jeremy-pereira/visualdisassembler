//
//  DisassemblyTests.swift
//  VisualDisassemblerTests
//
//  Created by Jeremy Pereira on 06/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import VisualDisassembler

class DisassemblyTests: XCTestCase
{

    override func setUpWithError() throws
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSimple() throws
	{
		let byteSequence: [UInt8] = [0x00, 0x01]
		var disassembly = Disassembly(parser: self, codeWriter: self)
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		if let code = disassembly.code
		{
			XCTAssert(code == byteSequence, "Got code: \(code)")
		}
		else
		{
			XCTFail("Got no code")
		}
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(output == "$00\n$01\n")
    }
}

extension DisassemblyTests: Parser
{

	func parse(program: ByteStream, info: [Info]) -> [UInt8]
	{
		var ret: [UInt8] = []

		while let next = program.next()
		{
			ret.append(next)
		}
		return ret
	}

	typealias Code = [UInt8]
}

extension DisassemblyTests: CodeWriter
{
	func write<Target>(_ code: [UInt8], to output: inout Target) where Target : TextOutputStream
	{
		let string = code.map { String(format: "$%02x\n", UInt($0)) }.joined()
		output.write(string)
	}
}

extension Array: CodeRepresentation where Element == UInt8
{
	public var errors: [DisassemblyError]
	{
		return []
	}

	public typealias Address = Int
}
