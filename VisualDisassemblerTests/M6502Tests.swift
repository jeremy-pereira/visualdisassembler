//
//  M6502Tests.swift
//  VisualDisassemblerTests
//
//  Created by Jeremy Pereira on 12/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import VisualDisassembler

private let log = Logger.getLogger("VisualDisassemblerTests.M6502Tests")

class M6502Tests: XCTestCase
{

    override func setUpWithError() throws
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	private func makeDisassembler() -> Disassembly<M6502.Disassembler, M6502.CA65Writer>
	{
		var disassembly = M6502.makeCA65Disassembler()
		disassembly.reportError = { XCTFail("\($0)") }
		disassembly.info =
		[
			.global(startAddress: "$03ff", cpu: "6502", outputFormat: "ca65")
		]
		return disassembly
	}

    func testSimpleDisassemble() throws
	{
		let expectedOutput = """
		        brk     ; 03ff

		"""
		let byteSequence: [UInt8] = [0x00]
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(containsIgnoringWhiteSpace(output: output, expected: expectedOutput),
				  "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")
    }

	func testARealProgram()
	{
		log.pushLevel(.info)
		defer { log.popLevel() }
		// change this to the URL of a program that exists. If it doesn't the
		// test just exits
		let progUrl = URL(string: "file:///Users/jeremyp/dev/M6502/space-invaders/space_invaders.prg")!
		let outputUrl = URL(fileURLWithPath: "testout.s").absoluteURL
		log.info("Disassembling to \(outputUrl)")
		do
		{
			let program = try Data(contentsOf: progUrl)
			var disassembly = makeDisassembler()
			disassembly.info =
			[
				.global(startAddress: "$03ff", cpu: "6502", outputFormat: "ca65"),
				.label(name: "showSoundWiring", address: "$0412", isCode: true, comment: "Shoews a screen with the wiring diagram"),
				.range(name: "loadAddress", start: "$03ff", end: "$0400", type: "Word", comment: "Basic load address"),
				.range(name: "basicBootstrap", start: "$0401", end: "$040e", type: "Byte", comment: "Basic bootstrap code"),
				.label(name: "entryPoint", address: "$040f", isCode: true, comment: "Machine code starts here"),
			]
			disassembly.program = AnyRandomAccessCollection(program)
			var output = ""
			disassembly.write(to: &output)
			try output.write(to: outputUrl, atomically: false, encoding: .utf8)
		}
		catch
		{
			log.error("\(error)")
		}
	}

	func testSta51()
	{
		let expectedOutput = """
		        lda     #$1c                            ; 03ff
		        sta     $51                             ; 0401
		"""
		let byteSequence: [UInt8] = [0xa9, 0x1c, 0x85, 0x51]
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(output.contains(expectedOutput), "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")
	}

	func testldy00()
	{
		let expectedOutput = """
		        ldy     #$00                            ; 03ff

		"""
		let byteSequence: [UInt8] = [0xa0, 0x00]
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(output == expectedOutput, "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")
	}

	func testbnef9()
	{
		let expectedOutput = """
		        bne     $f9                             ; 03ff

		"""
		let byteSequence: [UInt8] = [0xd0, 0xf9]
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(output == expectedOutput, "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")
	}

	func testInc()
	{
		let expectedOutput = """
			inc $51	; 03ff
		"""
		let byteSequence: [UInt8] = [0xe6, 0x51]
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(containsIgnoringWhiteSpace(output: output, expected: expectedOutput),
				  "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")
	}

	func testJsr()
	{
		let expectedOutput = """
			jsr $ffe4	; 03ff

		"""
		let byteSequence: [UInt8] = [0x20, 0xe4, 0xff]
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(containsIgnoringWhiteSpace(output: output, expected: expectedOutput),
				  "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")
	}

	func testInstructions()
	{
		runInstructionTest("ADC", bytes: [0x61, 0x00], expected: "\tadc ($00,x)")
		runInstructionTest("AND", bytes: [0x21, 0x00], expected: "\tand ($00,x)")
		runInstructionTest("ASL", bytes: [0x06, 0x00], expected: "\tasl $00")
		runInstructionTest("ASL A", bytes: [0x0a], expected: "\tasl a")
		runInstructionTest("BCC", bytes: [0x90, 0x00], expected: "\tbcc $00")
		runInstructionTest("BCS", bytes: [0xB0, 0x00], expected: "\tbcs $00")
		runInstructionTest("BEQ", bytes: [0xf0, 0x00], expected: "\tbeq $00")
		runInstructionTest("BIT zp", bytes: [0x24, 0x01], expected: "\tbit $01")
		runInstructionTest("BIT abs", bytes: [0x2c, 0x00, 0x01], expected: "\tbit $0100")
		runInstructionTest("BMI", bytes: [0x30, 0x00], expected: "\tbmi $00")
		runInstructionTest("BNE", bytes: [0xd0, 0x00], expected: "\tbne $00")
		runInstructionTest("BPL", bytes: [0x10, 0x00], expected: "\tbpl $00")
		runInstructionTest("BRK", bytes: [0x00], expected: "\tbrk")
		runInstructionTest("BVC", bytes: [0x50, 0x00], expected: "\tbvc $00")
		runInstructionTest("BVS", bytes: [0x70, 0x00], expected: "\tbvs $00")
		runInstructionTest("CLC", bytes: [0x18], expected: "\tclc")
		runInstructionTest("CLD", bytes: [0xd8], expected: "\tcld")
		runInstructionTest("CLI", bytes: [0x58], expected: "\tcli")
		runInstructionTest("CLV", bytes: [0xb8], expected: "\tclv")
		runInstructionTest("CMP", bytes: [0xc1, 0x00], expected: "\tcmp ($00,x)")
		runInstructionTest("CPX", bytes: [0xe0, 0x00], expected: "\tcpx #$00")
		runInstructionTest("CPY", bytes: [0xc0, 0x00], expected: "\tcpy #$00")
		runInstructionTest("DEC", bytes: [0xc6, 0x00], expected: "\tdec $00")
		runInstructionTest("DEX", bytes: [0xca], expected: "\tdex")
		runInstructionTest("DEY", bytes: [0x88], expected: "\tdey")
		runInstructionTest("EOR", bytes: [0x49, 0x00], expected: "\teor #$00")
		runInstructionTest("INC", bytes: [0xe6, 0x00], expected: "\tinc $00")
		runInstructionTest("INX", bytes: [0xe8], expected: "\tinx")
		runInstructionTest("INY", bytes: [0xc8], expected: "\tiny")
		runInstructionTest("JMP", bytes: [0x4c, 0x00, 0x00], expected: "\tjmp $0000")
		runInstructionTest("JSR", bytes: [0x20, 0x00, 0x00], expected: "\tjsr $0000")
		runInstructionTest("LDA", bytes: [0xa1, 0x00], expected: "\tlda ($00,x)")
		runInstructionTest("LDX", bytes: [0xbe, 0x00, 0x00], expected: "\tldx $0000,y")
		runInstructionTest("LDX", bytes: [0xb6, 0x10], expected: "\tldx $10,y")
		runInstructionTest("LDY", bytes: [0xb4, 0x00], expected: "\tldy $00,x")
		runInstructionTest("LSR", bytes: [0x46, 0x00], expected: "\tlsr $00")
		runInstructionTest("LSR A", bytes: [0x4a], expected: "\tlsr a")
		runInstructionTest("NOP", bytes: [0xea], expected: "\tnop")
		runInstructionTest("ORA", bytes: [0x11, 0x11], expected: "\tora ($11),y")
		runInstructionTest("PHA", bytes: [0x48], expected: "\tpha")
		runInstructionTest("PHP", bytes: [0x08], expected: "\tphp")
		runInstructionTest("PLA", bytes: [0x68], expected: "\tpla")
		runInstructionTest("PLP", bytes: [0x28], expected: "\tplp")
		runInstructionTest("ROL", bytes: [0x26, 0x00], expected: "\trol $00")
		runInstructionTest("ROL A", bytes: [0x2a], expected: "\trol a")
		runInstructionTest("ROR", bytes: [0x66, 0x00], expected: "\tror $00")
		runInstructionTest("ROR A", bytes: [0x6a], expected: "\tror a")
		runInstructionTest("RTI", bytes: [0x40], expected: "\trti")
		runInstructionTest("RTS", bytes: [0x60], expected: "\trts")
		runInstructionTest("SBC", bytes: [0xe1, 0x00], expected: "\tsbc ($00,x)")
		runInstructionTest("SEC", bytes: [0x38], expected: "\tsec")
		runInstructionTest("SED", bytes: [0xf8], expected: "\tsed")
		runInstructionTest("SEI", bytes: [0x78], expected: "\tsei")
		runInstructionTest("STA", bytes: [0x81, 0x00], expected: "\tsta ($00,x)")
		runInstructionTest("STX", bytes: [0x8e, 0x00, 0x00], expected: "\tstx $0000")
		runInstructionTest("STY", bytes: [0x94, 0x00], expected: "\tsty $00,x")
		runInstructionTest("TAX", bytes: [0xaa], expected: "\ttax")
		runInstructionTest("TAY", bytes: [0xa8], expected: "\ttay")
		runInstructionTest("TYA", bytes: [0x98], expected: "\ttya")
		runInstructionTest("TSX", bytes: [0xba], expected: "\ttsx")
		runInstructionTest("TXA", bytes: [0x8a], expected: "\ttxa")
		runInstructionTest("TXS", bytes: [0x9a], expected: "\ttxs")
	}

	func runInstructionTest(_ instruction: String, bytes: [UInt8], expected: String)
	{
		var disassembly = makeDisassembler()
		disassembly.program = AnyRandomAccessCollection(bytes)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(containsIgnoringWhiteSpace(output: output, expected: expected),
				  "\(instruction): Incorrect output, got: \n\"\"\"\n\(output)\"\"\"\n expected: \n\"\"\"\n\(expected)...\n\"\"\"")
	}

	func testUnbreakingLabels()
	{
		let expectedOutput = """
			.byte $81	; 03ff
		start:
			jsr $ffe4	; 0400

		"""
		let byteSequence: [UInt8] = [0x81, 0x20, 0xe4, 0xff]
		let symbols: [Info] = [
			.global(startAddress: "$03ff", cpu: "6502", outputFormat: "ca65"),
			.label(name: "start", address: "$0400", isCode: true, comment: nil),
		]
		var disassembly = makeDisassembler()
		disassembly.info = symbols
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(containsIgnoringWhiteSpace(output: output, expected: expectedOutput),
				  "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")

	}

	func testRanges()
	{
		let expectedOutput = """
			.byte $81	; 03ff
			jsr $ffe4	; 0400

		"""
		let byteSequence: [UInt8] = [0x81, 0x20, 0xe4, 0xff]
		let symbols: [Info] = [
			.global(startAddress: "$03ff", cpu: "6502", outputFormat: "ca65"),
			.range(name: nil, start: "$03ff", end: "$03ff", type: "byte", comment: nil)
		]
		var disassembly = makeDisassembler()
		disassembly.info = symbols
		disassembly.program = AnyRandomAccessCollection(byteSequence)
		var output: String = ""
		disassembly.write(to: &output)
		XCTAssert(containsIgnoringWhiteSpace(output: output, expected: expectedOutput),
				  "Incorrect output, got: \n\"\"\"\n\(output)\"\"\"")

	}

	private func containsIgnoringWhiteSpace(output: String, expected: String) -> Bool
	{
		let strippedOutput = output.replacingOccurrences(of: "\\s+", with: "\t", options: .regularExpression)
		let strippedExpected = expected.replacingOccurrences(of: "\\s+", with: "\t", options: .regularExpression)
		return strippedOutput.contains(strippedExpected)
	}

}
