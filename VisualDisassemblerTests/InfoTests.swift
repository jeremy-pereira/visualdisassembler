//
//  InfoTests.swift
//  VisualDisassemblerTests
//
//  Created by Jeremy Pereira on 30/04/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
import Toolbox
@testable import VisualDisassembler

private let log = Logger.getLogger("VisualDisassemblerTests.InfoTests")

class InfoTests: XCTestCase
{

    override func setUpWithError() throws
	{
		log.level = .debug
    }

    override func tearDownWithError() throws
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEncode() throws
	{
		let info: [Info] =
		[
			.global(startAddress: "$03ff", cpu: "6502", outputFormat: "ca65"),
			.label(name: "showSoundWiring", address: "$0412", isCode: true, comment: "Shoews a screen with the wiring diagram"),
			.range(name: "loadAddress", start: "$03ff", end: "$0400", type: "Byte", comment: "Basic load address"),
			.range(name: "basicBootstrap", start: "$0401", end: "$040e", type: "Byte", comment: "Basic bootstrap code"),
			.label(name: "entryPoint", address: "$040f", isCode: true, comment: "Machine code starts here"),
		]
		let encoder = JSONEncoder()
		encoder.outputFormatting = [.prettyPrinted, .sortedKeys]
		do
		{
			let output = try encoder.encode(info)
			if log.isDebug
			{
				let outputString = String(data: output, encoding: .utf8)!
				log.debug(outputString)
			}

		}
		catch
		{
			XCTFail("\(error)")
		}
    }

	func testDecode()
	{
		let string = """
[
  {
    "global" : {
      "cpu" : "6502",
      "outputFormat" : "ca65",
      "startAddress" : "$03ff"
    }
  },
  {
    "label" : {
      "address" : "$0412",
      "comment" : "Shows a screen with the wiring diagram",
      "isCode" : true,
      "name" : "showSoundWiring"
    }
  },
  {
    "range" : {
      "comment" : "Basic load address",
      "end" : "$0400",
      "name" : "loadAddress",
      "start" : "$03ff",
      "type" : "Byte"
    }
  },
  {
    "range" : {
      "comment" : "Basic bootstrap code",
      "end" : "$040e",
      "name" : "basicBootstrap",
      "start" : "$0401",
      "type" : "Byte"
    }
  },
  {
    "label" : {
      "address" : "$040f",
      "comment" : "Machine code starts here",
      "isCode" : true,
      "name" : "entryPoint"
    }
  }
]
"""
		let data = Data(string.utf8)
		let decoder = JSONDecoder()
		do
		{
			let info = try decoder.decode([Info].self, from: data)
			XCTAssert(info.count == 5)
			log.debug("\(info)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}
}
