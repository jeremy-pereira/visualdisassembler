//
//  PatternTests.swift
//  VisualDisassemblerTests
//
//  Created by Jeremy Pereira on 20/03/2020.
//  Copyright © 2020 Jeremy Pereira. All rights reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

import XCTest
@testable import VisualDisassembler

class PatternTests: XCTestCase
{

    override func setUp()
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSimplePattern()
	{
		let testBytes = ByteStream(sequence: [0x00, 0x01])
		let pattern = Pattern(byteOrder: .byte, elements: [Pattern.Element.Constant(0, bits: 0 ..< 8)])
		let result = pattern.match(stream: testBytes)
		XCTAssert(result != nil, "Failed to match first byte")
		XCTAssert(pattern.match(stream: testBytes) == nil, "Accidentally matched second byte")
    }

    func testField()
	{
		let testBytes = ByteStream(sequence: [0x0c, 0x01])
		let pattern = Pattern(byteOrder: .byte, elements: [Pattern.Element.Field("label", bits: 2 ..< 4)])
		guard let result = pattern.match(stream: testBytes) else { XCTFail("Failed to match first byte") ; return }
		guard let labelValue = result["label"] else { XCTFail("no entry  for 'label' in \(result)") ; return }
		XCTAssert(labelValue == 3)
    }

}
